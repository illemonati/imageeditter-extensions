/* tslint:disable */
/* eslint-disable */
/**
* @param {number} point_x 
* @param {number} point_y 
* @param {number} tolerence 
* @param {Uint8Array} image_data 
* @param {Uint8Array} out_data 
* @param {number} length 
* @param {number} image_width 
* @param {number} image_height 
* @param {number} r 
* @param {number} g 
* @param {number} b 
* @param {number} a 
*/
export function flood_fill(point_x: number, point_y: number, tolerence: number, image_data: Uint8Array, out_data: Uint8Array, length: number, image_width: number, image_height: number, r: number, g: number, b: number, a: number): void;
/**
* @param {number} point_x 
* @param {number} point_y 
* @param {number} tolerence 
* @param {Uint8Array} image_data 
* @param {number} length 
* @param {number} image_width 
* @param {number} image_height 
* @returns {Uint32Array} 
*/
export function flood_fill_get_border(point_x: number, point_y: number, tolerence: number, image_data: Uint8Array, length: number, image_width: number, image_height: number): Uint32Array;
/**
* @param {number} point_x 
* @param {number} point_y 
* @param {number} tolerence 
* @param {Uint8Array} image_data 
* @param {Uint8Array} out_data 
* @param {number} length 
* @param {number} image_width 
* @param {number} image_height 
* @param {number} r 
* @param {number} g 
* @param {number} b 
* @param {number} a 
*/
export function flood_fill_variance(point_x: number, point_y: number, tolerence: number, image_data: Uint8Array, out_data: Uint8Array, length: number, image_width: number, image_height: number, r: number, g: number, b: number, a: number): void;
/**
* @param {number} point_x 
* @param {number} point_y 
* @param {number} tolerence 
* @param {Uint8Array} image_data 
* @param {Uint8Array} out_data 
* @param {number} length 
* @param {number} image_width 
* @param {number} image_height 
* @param {number} r 
* @param {number} g 
* @param {number} b 
* @param {number} a 
*/
export function cool_fill(point_x: number, point_y: number, tolerence: number, image_data: Uint8Array, out_data: Uint8Array, length: number, image_width: number, image_height: number, r: number, g: number, b: number, a: number): void;

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly flood_fill: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number, l: number, m: number, n: number) => void;
  readonly flood_fill_get_border: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number) => void;
  readonly flood_fill_variance: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number, l: number, m: number, n: number) => void;
  readonly cool_fill: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number, l: number, m: number, n: number) => void;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
}

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
        