/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function flood_fill(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number, l: number, m: number, n: number): void;
export function flood_fill_get_border(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number): void;
export function flood_fill_variance(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number, l: number, m: number, n: number): void;
export function cool_fill(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number, l: number, m: number, n: number): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
