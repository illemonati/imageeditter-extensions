import Extension from '../extension.js';
import * as wasm from './wasm/flood_fill.js';
import wasm_init from './wasm/flood_fill.js';

//@ts-ignore
// import Vue from '/imageediter/vue.js';
import 'https://cdn.jsdelivr.net/npm/vue@2.6.11';
//@ts-ignore
// import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.11/dist/vue.esm.browser.js';
//@ts-ignore
// window.Vue = Vue;
// //@ts-ignore
// import 'https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.js';
// import Vuetify from 'https://cdn.pika.dev/vuetify@1.5.24';
//@ts-ignore
// import Vuetify from 'https://cdn.pika.dev/vuetify@1.5.24';
//@ts-ignore
// import Vuetify from 'https://cdn.pika.dev/vuetify@^2.3.0-beta.1';
//@ts-ignore
// import * as Vuetify from 'https://unpkg.com/vuetify@1.5.18/dist/vuetify.js';
import 'https://unpkg.com/vuetify@2.2.20/dist/vuetify.js';
// import Vuetify from 'https://unpkg.com/vuetify@1.5.24/dist/vuetify.min.js';

const config = {
    author: 'Tong Miao',
    email: 'tonymiaotong@tioft.tech',
    domain: 'tongmiao.cloud',
    name: 'Flood Fill',
    description:
        'This extension will allow a user to click on part of a image and fill it in',
    version: '0.0.1',
    buttonIcon: 'floodfill/buttonicon.svg',
    settings: 'floodfill/settings.html',
};

interface FloodFillSettings {
    r: number;
    g: number;
    b: number;
    a: number;
    tolerence: number;
}

interface OtherFloodFillSettings {
    variance: boolean;
    createSelection: boolean;
}

class FloodFillExtension extends Extension {
    canvas: HTMLCanvasElement | undefined;
    floodFillSettings: FloodFillSettings | undefined;
    settingsDiv: HTMLDivElement | undefined;
    vueApp: any;
    otherFloodFillSettings: OtherFloodFillSettings | undefined;

    mainFill(e: MouseEvent) {
        const box = this.canvas!.getBoundingClientRect();
        let x = e.clientX - box.left;
        let y = e.clientY - box.top;
        x /= this.scale / 100;
        y /= this.scale / 100;
        const floodFillSettings = this.floodFillSettings;
        const ctx = this.canvas!.getContext('2d')!;
        // try {
        const imageData = ctx.getImageData(
            0,
            0,
            this.canvas!.width,
            this.canvas!.height
        );
        if (this.otherFloodFillSettings?.createSelection) {
            const pointsOnEdge = wasm.flood_fill_get_border(
                x,
                y,
                floodFillSettings!.tolerence ** 2,
                (imageData.data as unknown) as Uint8Array,
                imageData.data.length,
                imageData.width,
                imageData.height
                // floodFillSettings!.r,
                // floodFillSettings!.g,
                // floodFillSettings!.b,
                // floodFillSettings!.a
            );
            console.log(pointsOnEdge);
            const overlayGenerationCanvas = document.createElement('canvas');
            overlayGenerationCanvas.height = this.canvas!.height;
            overlayGenerationCanvas.width = this.canvas!.width;
            const ctx = overlayGenerationCanvas.getContext('2d')!;
            const overlayImageData = ctx.getImageData(
                0,
                0,
                overlayGenerationCanvas.width,
                overlayGenerationCanvas.height
            );
            for (let i = 0; i < pointsOnEdge.length; i += 2) {
                const x = pointsOnEdge[i + 1];
                const y = pointsOnEdge[i];
                const index = (x * y + x) * 4;
                overlayImageData.data[index] = floodFillSettings!.r;
                overlayImageData.data[index + 1] = floodFillSettings!.g;
                overlayImageData.data[index + 2] = floodFillSettings!.b;
                // overlayImageData.data[index + 3] = floodFillSettings!.a;
            }
            ctx.putImageData(overlayImageData, 0, 0);
            const image = overlayGenerationCanvas.toDataURL('image/png');
            console.log(image);
            return;
        }
        if (this.otherFloodFillSettings?.variance) {
            wasm.flood_fill_variance(
                x,
                y,
                floodFillSettings!.tolerence ** 2,
                (imageData.data as unknown) as Uint8Array,
                (imageData.data as unknown) as Uint8Array,
                imageData.data.length,
                imageData.width,
                imageData.height,
                floodFillSettings!.r,
                floodFillSettings!.g,
                floodFillSettings!.b,
                floodFillSettings!.a
            );
        } else {
            wasm.flood_fill(
                x,
                y,
                floodFillSettings!.tolerence ** 2,
                (imageData.data as unknown) as Uint8Array,
                (imageData.data as unknown) as Uint8Array,
                imageData.data.length,
                imageData.width,
                imageData.height,
                floodFillSettings!.r,
                floodFillSettings!.g,
                floodFillSettings!.b,
                floodFillSettings!.a
            );
        }
        ctx.putImageData(imageData, 0, 0);
        // } catch (e) {
        //     // Dont do any thing
        // }
    }

    changeSettings(extension: FloodFillExtension) {
        return (settings: FloodFillSettings) => {
            extension.floodFillSettings = settings;
        };
    }

    initFloodFillSettings() {
        this.floodFillSettings = {
            r: 255,
            g: 255,
            b: 255,
            a: 255,
            tolerence: 50,
        };
        this.otherFloodFillSettings = {
            variance: true,
            createSelection: false,
        };
    }

    select(e: any) {
        super.select(e);
        this.settingsDiv = document.querySelector(
            '#floodFillSettingsDiv'
        ) as HTMLDivElement;

        //@ts-ignore
        this.vueApp = new Vue({
            el: this.settingsDiv,
            //@ts-ignore
            vuetify: new Vuetify({
                theme: {
                    dark: true,
                },
            }),
            data: {
                floodFillSettings: this.floodFillSettings!,
                otherFloodFillSettings: this.otherFloodFillSettings!,
                display: 'none',
                pastSettings: [] as FloodFillSettings[],
            },
            mounted: function () {
                this.loadPastSettings();
                this.$nextTick(() => {
                    const self = this;

                    self.display = 'flex';
                });
            },
            methods: {
                capitalizeFirstLetter(string: string) {
                    return string.charAt(0).toUpperCase() + string.slice(1);
                },
                savePastSettings() {
                    window.localStorage.setItem(
                        'floodFillPastSettings',
                        //@ts-ignore
                        btoa(JSON.stringify(this.pastSettings))
                    );
                },
                loadPastSettings() {
                    const pastSettings = window.localStorage.getItem(
                        'floodFillPastSettings'
                    );
                    try {
                        if (pastSettings) {
                            //@ts-ignore
                            this.pastSettings = JSON.parse(atob(pastSettings));
                        }
                    } catch (e) {}
                },
                getValidationRules(setting: string) {
                    const rgbInputRules = [
                        (v: string) => {
                            const parsed = parseInt(v);
                            return (
                                (parsed > -1 && parsed < 256) ||
                                'Value must be an integer between 0 and 255'
                            );
                        },
                    ];
                    const toleranceInputRules = [
                        (v: string) => {
                            const parsed = parseInt(v);
                            return (
                                parsed > -1 ||
                                'Value must be an integer above 0'
                            );
                        },
                    ];
                    return ['r', 'g', 'b'].includes(setting)
                        ? rgbInputRules
                        : toleranceInputRules;
                },
                getColor(setting: string) {
                    switch (setting) {
                        case 'r':
                            return 'red';
                        case 'g':
                            return 'green';
                        case 'b':
                            return 'blue';
                        default:
                            return 'purple';
                    }
                },
                getHexColor(setting: string) {
                    switch (setting) {
                        case 'r':
                            return '#F44336';
                        case 'g':
                            return '#4CAF50';
                        case 'b':
                            return '#2196F3';
                        default:
                            return '#9C27B0';
                    }
                },
                changeFloodFillSettings(settings: FloodFillSettings) {
                    //@ts-ignore
                    Object.assign(this.floodFillSettings, settings);
                },
                rgbToHex(r: number, g: number, b: number) {
                    return (
                        '#' +
                        [r, g, b]
                            .map((x) => {
                                const hex = x.toString(16);
                                return hex.length === 1 ? '0' + hex : hex;
                            })
                            .join('')
                    );
                },
                saveFloodFillSettings() {
                    //@ts-ignore
                    const settings = this.floodFillSettings;
                    //@ts-ignore
                    this.pastSettings.unshift({ ...settings });
                    //@ts-ignore
                    this.pastSettings.length > 5 &&
                        //@ts-ignore
                        this.pastSettings.pop();
                    //@ts-ignore
                    this.savePastSettings(this.pastSettings);
                },
            },
            computed: {
                //@ts-ignore
                rColor: function () {
                    return {
                        //@ts-ignore
                        color: `rgb(${this.floodFillSettings.r}, 0, 0)`,
                    };
                },
                //@ts-ignore
                gColor: function () {
                    return {
                        //@ts-ignore
                        color: `rgb(0, ${this.floodFillSettings.g}, 0)`,
                    };
                },
                //@ts-ignore
                bColor: function () {
                    return {
                        //@ts-ignore
                        color: `rgb(0, 0, ${this.floodFillSettings.b})`,
                    };
                },
                //@ts-ignore
                allColor: function () {
                    return {
                        //@ts-ignore
                        color: `rgb(${this.floodFillSettings.r}, ${this.floodFillSettings.g}, ${this.floodFillSettings.b})`,
                    };
                },
            },
            watch: {},
        });
    }

    async onload($container: HTMLElement) {
        // Vue.use(Vuetify);
        this.canvas = this.$canvas;
        // console.log(Vuetify);
        super.onload($container);
        await wasm_init();
        const css = document.createElement('link');
        css.href = '/libs/imageediter/vuetify.css';
        css.rel = 'stylesheet';
        document.body.appendChild(css);
        const iconStyles = document.createElement('link');
        iconStyles.rel = 'stylesheet';
        iconStyles.href =
            'https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css';
        document.body.appendChild(iconStyles);

        this.initFloodFillSettings();
    }

    imageclick(e: MouseEvent) {
        if (!super.imageclick(e)) {
            return false;
        }

        this.mainFill(e);
        return true;
    }
}

const floodFill = new FloodFillExtension(config);

export default floodFill;
