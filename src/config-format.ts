export interface ExtensionConfig {
    author: string;
    email: string;
    domain: string;
    name: string;
    description: string;
    version: string;
    buttonIcon: string;
    settings: string;
}
