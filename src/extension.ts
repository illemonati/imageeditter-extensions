import { ExtensionConfig } from './config-format';

const stack = [];
let currentExtension: string | null = null;

class Extension {
    buttonIcon: URL;
    $canvas: HTMLCanvasElement;
    name: string;
    settings: string;
    settingsHtml: string | undefined;
    $section: HTMLElement;
    $settings: HTMLElement;
    // settingsShadow: ShadowRoot;

    constructor(config: ExtensionConfig) {
        if (!config.buttonIcon) throw new Error('Please provide a button icon');
        this.buttonIcon = new URL(
            `${location.origin}/libs/imageediter/extensions/${config.buttonIcon}`
        );
        this.$canvas = document.querySelector('canvas')!;
        this.name = config.name;
        this.$section = document.querySelector('section')!;
        // if (!this.$section.shadowRoot) {
        //     this.settingsShadow = this.$section.attachShadow({ mode: 'open' });
        //     this.$settings = document.createElement('div');
        //     this.settingsShadow.appendChild(this.$settings);
        // } else {
        //     this.settingsShadow = this.$section.shadowRoot;
        //     this.$settings = this.settingsShadow
        //         .firstElementChild as HTMLElement;
        // }
        this.$settings = this.$section;
        this.settings = config.settings;
    }

    async onload($container: HTMLElement) {
        const $button = document.createElement('button');
        $button.innerHTML = `<img src='${this.buttonIcon}'>`;
        $container.appendChild($button);

        $button.addEventListener('click', this.select.bind(this));

        const self = this;
        this.$canvas.addEventListener('click', function (e) {
            self.imageclick(e);
        });

        if (this.settings) {
            fetch(`/imageediter/extensions/${this.settings}`)
                .then((res) => {
                    return res.text();
                })
                .then((data) => {
                    self.settingsHtml = data;
                });
        }
    }

    select(e: any) {
        currentExtension = this.name;
        this.settingsHtml && (this.$settings.innerHTML = this.settingsHtml);
    }

    imageclick(e: MouseEvent) {
        if (this.name !== currentExtension) return false;
        /*
		1. Save the current image to the stack
		2. Do our Operation
		3. Display the result
		*/
        return true;
    }
    get scale() {
        return parseInt(document.getElementById('zoomlabel')!.innerText);
    }
}

export default Extension;
