/* tslint:disable */
/* eslint-disable */
/**
* @param {Uint16Array} in_hsv_array 
* @param {Uint8Array} out_rgb_array 
* @param {number} len 
*/
export function convert_hsv_to_rgb_wasm(in_hsv_array: Uint16Array, out_rgb_array: Uint8Array, len: number): void;
/**
* @param {Uint8Array} in_rgb_array 
* @param {Uint16Array} out_hsv_array 
* @param {number} len 
*/
export function convert_rgb_to_hsv_wasm(in_rgb_array: Uint8Array, out_hsv_array: Uint16Array, len: number): void;
/**
* @param {Uint16Array} in_hsv_array 
* @param {Uint16Array} out_hsv_array 
* @param {number} hue_multiplier 
* @param {number} hue_range 
* @param {number} saturation_multiplier 
* @param {number} saturation_range 
* @param {number} value_multipler 
* @param {number} value_range 
* @param {number} array_len 
*/
export function adjust_image_hsv_wasm(in_hsv_array: Uint16Array, out_hsv_array: Uint16Array, hue_multiplier: number, hue_range: number, saturation_multiplier: number, saturation_range: number, value_multipler: number, value_range: number, array_len: number): void;
/**
* @param {Uint8Array} in_rgb_array 
* @param {Uint8Array} out_rgb_array 
* @param {number} hue_multiplier 
* @param {number} hue_range 
* @param {number} saturation_multiplier 
* @param {number} saturation_range 
* @param {number} value_multipler 
* @param {number} value_range 
* @param {number} array_len 
*/
export function adjust_image_hsv_from_rgb_wasm(in_rgb_array: Uint8Array, out_rgb_array: Uint8Array, hue_multiplier: number, hue_range: number, saturation_multiplier: number, saturation_range: number, value_multipler: number, value_range: number, array_len: number): void;

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly convert_hsv_to_rgb_wasm: (a: number, b: number, c: number, d: number, e: number) => void;
  readonly convert_rgb_to_hsv_wasm: (a: number, b: number, c: number, d: number, e: number) => void;
  readonly adjust_image_hsv_wasm: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number) => void;
  readonly adjust_image_hsv_from_rgb_wasm: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number) => void;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
}

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
        