/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function convert_hsv_to_rgb_wasm(a: number, b: number, c: number, d: number, e: number): void;
export function convert_rgb_to_hsv_wasm(a: number, b: number, c: number, d: number, e: number): void;
export function adjust_image_hsv_wasm(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number): void;
export function adjust_image_hsv_from_rgb_wasm(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
